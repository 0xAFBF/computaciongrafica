//
//  Geom.cpp
//  ComputacionGrafica
//
//  Created by Andres Botero on 5/7/14.
//  Copyright (c) 2014 BoteRock. All rights reserved.
//

#include "Geom.h"


float clamp(float num, float minimum, float maximum)
{
	if( num<minimum) return minimum;
	if( num>maximum) return maximum;
	return num;
}

bool nearf(float num, float target, float interval)
{
  if ((num>target-interval)&&(num<target+interval)) {
    return true;
  }
  return false;
}

Vector3 vec3(float x, float y, float z)
{
	Vector3 v = {x, y, z};
  return v;
}

Vector3 crossp(Vector3 v1, Vector3 v2)
{
	Vector3 r;
	r.x = v1.y*v2.z - v2.y*v1.z;
	r.y = -(v1.x*v2.z - v2.x*v1.z);
	r.z = v1.x*v2.y - v2.x*v1.y;
	return r;
}

Vector3 normalize(Vector3 v)
{
  float mag = sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
  v.x /= mag;
  v.y /= mag;
  v.z /= mag;
  return v;
}

Vector3 delta(Vector3 v1, Vector3 v2)
{
  v2.x -= v1.x;
  v2.y -= v1.y;
  v2.z -= v1.z;
  return v2;
}

