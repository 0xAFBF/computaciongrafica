//
//  Character.h
//  ComputacionGrafica
//
//  Created by Andres Botero on 5/5/14.
//  Copyright (c) 2014 BoteRock. All rights reserved.
//


#include <math.h>

#include "Geom.h"
#include "glm.h"
#include "GameObject.h"
#ifndef __ComputacionGrafica__Character__
#define __ComputacionGrafica__Character__



class Character :public GameObject{
//  instance variables
  Vector3 heading;
  float angle;
  
  bool idle;
  
  float speed = 3.0f;
  
  GLMmodel *personaje;
  GLMmodel *personajebrazoR;
  GLMmodel *personajebrazoL;
  GLMmodel *personajepiernaR;
  GLMmodel *personajepiernaL;
  GLMmodel *personajetronco;
  
  float tT = 0.f;
  
  
public:
  
  Vector3 pos;
  
//  member functions
  Character(Vector3 _pos, Vector3 _heading, float _speed );
  Character();
  
  void draw();
  void refresh(float deltaTime);
  void move(float x, float y);
  void reset();
};


#endif /* defined(__ComputacionGrafica__Character__) */
