//
//  Guillotina.h
//  ComputacionGrafica
//
//  Created by Andres Botero on 5/14/14.
//  Copyright (c) 2014 BoteRock. All rights reserved.
//

#ifndef __ComputacionGrafica__Guillotina__
#define __ComputacionGrafica__Guillotina__

#include <math.h>

#include "Geom.h"
#include "glm.h"
#include "GameObject.h"

#include "Character.h"

class Guillotina :public GameObject{
  //  instance variables
  
  
  GLMmodel *guillotina;
  
  float tT = 0.f;
  
  
public:
  //  member functions
  
  Guillotina();
  
  void draw();
  void refresh(float deltaTime);
  void checkDeath(Character*);
  
};


#endif /* defined(__ComputacionGrafica__Guillotina__) */
