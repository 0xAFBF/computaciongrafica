//
//  GameObject.h
//  ComputacionGrafica
//
//  Created by Andres Botero on 5/7/14.
//  Copyright (c) 2014 BoteRock. All rights reserved.
//

#ifndef ComputacionGrafica_GameObject_h
#define ComputacionGrafica_GameObject_h

class GameObject
{
public:
  
  virtual void draw() = 0;
  virtual void refresh(float deltaTime) = 0;
  
};

#endif
