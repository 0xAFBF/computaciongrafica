//
//  ViewUtils.h
//  ComputacionGrafica
//
//  Created by Andres Botero on 5/7/14.
//  Copyright (c) 2014 BoteRock. All rights reserved.
//

#ifndef ComputacionGrafica_ViewUtils_h
#define ComputacionGrafica_ViewUtils_h

void VerTexto( float x, float y, float z, void *font, const char *text );

void verEjes( float size );

void verGrilla( float max, float delta );

void verPlano( bool grid, bool axes, float size, float deltaGrid );


#endif
