//
//  Character.cpp
//  ComputacionGrafica
//
//  Created by Andres Botero on 5/5/14.
//  Copyright (c) 2014 BoteRock. All rights reserved.
//

#include "Character.h"

Character::Character(Vector3 _pos, Vector3 _heading, float _speed )
{
  personaje = glmReadOBJ("personaje.obj");
  pos = _pos;
}

Character::Character()
{
  personaje = glmReadOBJ("personaje_completo.obj");
  personajebrazoR = glmReadOBJ("personaje_brazoDer.obj");
  personajebrazoL = glmReadOBJ("personaje_brazoIzq.obj");
  personajepiernaR = glmReadOBJ("personaje_piernaDer.obj");
  personajepiernaL = glmReadOBJ("personaje_piernaIzq.obj");
  personajetronco = glmReadOBJ("personaje_tronco.obj");
  
  pos = vec3(0,0,0);
}

void Character::draw(){
  
  
  glTranslatef(pos.x, pos.y, 0);
  glRotatef(angle*180/M_PI -90, 0, 0, 1);
  
  if(idle)
  {
//    glPushMatrix();
    
    glmDraw( personaje, GLM_SMOOTH | GLM_MATERIAL);
//    glPopMatrix();
  }
  else
  {
    
    glmDraw( personajetronco, GLM_SMOOTH | GLM_MATERIAL);
    
    glPushMatrix();
    glTranslatef(-0.1755, 0.1309, 1.403);
    glRotatef(sin(tT*10)*60, 1, 0, 0);
    glTranslatef(0.1755, -0.1309, -1.403);
    
    glmDraw( personajebrazoR, GLM_SMOOTH | GLM_MATERIAL);
    
    
    glPopMatrix();
    glPushMatrix();
    
    glTranslatef(-0.174, 0.1348, 1.3945);
    glRotatef(sin(tT*10 + M_PI)*60, 1, 0, 0);
    glTranslatef(0.174, -0.1348, -1.3945);
    
    glmDraw( personajebrazoL, GLM_SMOOTH | GLM_MATERIAL);
    
    glPopMatrix();
    glPushMatrix();
    
    glTranslatef(-0.1141, 0.0034, 0.8685);
    glRotatef(sin(tT*10)*40, 1, 0, 0);
    glTranslatef(0.1141, -0.0034, -0.8685);
    
    
    
    glmDraw( personajepiernaL, GLM_SMOOTH | GLM_MATERIAL);
    
    glPopMatrix();
    glPushMatrix();
    
    glTranslatef(0.1032, 0.0309, 0.8793);
    glRotatef(sin(tT*10+ M_PI)*40, 1, 0, 0);
    glTranslatef(-0.1032, -0.0309, -0.8793);
    
    glmDraw( personajepiernaR, GLM_SMOOTH | GLM_MATERIAL);
    glPopMatrix();
  }
  
}

void Character::refresh(float deltaTime){
  tT += deltaTime;
  
  if(!idle)
  {
    pos.x += heading.x * speed * deltaTime;
    pos.y += heading.y * speed * deltaTime;
    
    pos.y = clamp(pos.y, -2.5, 2.5);
  }
}

void Character::move(float x, float y){
  float mag = sqrtf(x*x + y*y);
  if (mag<0.1) {
    idle = true;
  }
  else
  {
    idle = false;
    heading.x = x;
    heading.y = y;
    
    angle = atan2f(y, x);
    
  }
}

void Character::reset()
{
  pos = vec3(0,0,0);
}
