//
//  figuras.h
//  ComputacionGrafica
//
//  Created by Andres Botero on 3/16/14.
//  Copyright (c) 2014 BoteRock. All rights reserved.
//

#include "Geom.h"

#ifndef ComputacionGrafica_figuras_h
#define ComputacionGrafica_figuras_h

void materialOro();
void materialPlata();
void materialCobre();
void materialVidrio();
void drawFace(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4);
void drawCylinder(float r1, float r2, float r3, float r4, float h, float seg, float theta0, float theta1, bool capTop, bool capBot);
void drawEllipsoid(float rx, float ry, float rz, int divX, int divY, float theta0, float theta1, float phi0, float phi1);
void drawTorus(float rx, float ry, int divX, int divY, float theta0, float theta1, float phi0, float phi1);
void drawRoundedCube( float sizeX, float sizeY, float sizeZ, float radius, int subdivs);


#endif
