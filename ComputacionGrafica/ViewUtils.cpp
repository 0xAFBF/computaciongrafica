//
//  ViewUtils.cpp
//  ComputacionGrafica
//
//  Created by Andres Botero on 5/7/14.
//  Copyright (c) 2014 BoteRock. All rights reserved.
//

#include "ViewUtils.h"

#ifdef __APPLE__
#include <GLUT/GLUT.h>
#else
#include <GL/glut.h>
#endif

#include <string.h>

void VerTexto( float x, float y, float z, void *font, const char *text )
{
  // Fuentes Posibles //
  /*
   GLUT_BITMAP_8_BY_13
   GLUT_BITMAP_9_BY_15
   GLUT_BITMAP_TIMES_ROMAN_10
   GLUT_BITMAP_TIMES_ROMAN_24
   GLUT_BITMAP_HELVETICA_10
   GLUT_BITMAP_HELVETICA_12
   GLUT_BITMAP_HELVETICA_18
   */
  int lon = (int) strlen(text);
  glRasterPos3f(x,y,z);
  for(int n=0; n<lon; n++)
    glutBitmapCharacter( font, text[n] );
}


void verEjes( float size )
{
  glBegin (GL_LINES);
  glColor4f (1.0, 0.0, 0.0, 0.7); glVertex3f(-size, 0.0, 0.0); glVertex3f(size, 0.0, 0.0);
  glColor4f (0.0, 1.0, 0.0, 0.7); glVertex3f(0.0, -size, 0.0); glVertex3f(0.0, size, 0.0);
  glColor4f (0.0, 0.0, 1.0, 0.7); glVertex3f(0.0, 0.0, -size); glVertex3f(0.0, 0.0, size);
  glEnd();
}


void verGrilla( float max, float delta )
{
  glPushMatrix();
  //glEnable(GL_LINE_STIPPLE);
  //glLineStipple( 2, 0x1111 );
  glBegin (GL_LINES);
  glColor4f (1.0, 1.0, 0.0, 0.4 );
  for( float x=delta; x<max; x+=delta )
  {
    glVertex3f( x, 0.0, 0.0); glVertex3f( x, max, 0.0);
    glVertex3f( max, x, 0.0); glVertex3f( 0.0, x, 0.0);
  }
  glColor4f ( 1.0, 0.0, 1.0, 0.4 );
  for( float x=delta; x<max; x+=delta )
  {
    glVertex3f( x, 0.0, 0.0); glVertex3f( x, 0.0, max);
    glVertex3f( max, 0.0, x); glVertex3f( 0.0, 0.0, x);
  }
  glColor4f ( 0.0, 1.0, 1.0, 0.4 );
  for( float x=delta; x<max; x+=delta )
  {
    glVertex3f( 0.0, 0.0, x); glVertex3f( 0.0, max, x);
    glVertex3f( 0.0, x, 0.0); glVertex3f( 0.0, x, max );
  }
  glEnd();
  glDisable(GL_LINE_STIPPLE);
  glPopMatrix();
}


void verPlano( bool grid, bool axes, float size, float deltaGrid )
{
  if(axes)verEjes( size );
  if(grid)verGrilla( size, deltaGrid );
  if(axes||grid)
  {
    glColor3f (1.0, 1.0, 1.0 );
    VerTexto( 11, 0, 0, GLUT_BITMAP_8_BY_13, "Eje X" );
    VerTexto( 0, 11, 0, GLUT_BITMAP_8_BY_13, "Eje Y" );
    VerTexto( 0, 0, 11, GLUT_BITMAP_8_BY_13, "Eje Z" );
  }
  //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
}

