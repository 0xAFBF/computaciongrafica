//
//  Tronco.h
//  ComputacionGrafica
//
//  Created by Andres Botero on 5/15/14.
//  Copyright (c) 2014 BoteRock. All rights reserved.
//

#ifndef __ComputacionGrafica__Tronco__
#define __ComputacionGrafica__Tronco__


#include <math.h>

#include "Geom.h"
#include "glm.h"
#include "GameObject.h"


class Tronco :public GameObject{
  //  instance variables
  
  
  GLMmodel *tronco;
  
  float tT = 0.f;
  
  
public:
  //  member functions
  
  Tronco();
  
  void draw();
  void refresh(float deltaTime);
};


#endif /* defined(__ComputacionGrafica__Tronco__) */
