//
//  Chuzos.cpp
//  ComputacionGrafica
//
//  Created by Andres Botero on 5/15/14.
//  Copyright (c) 2014 BoteRock. All rights reserved.
//

#include "Chuzos.h"
Chuzos::Chuzos()
{
  chuzos = glmReadOBJ("trap_puas.obj");
}

void Chuzos::draw(){
  glPushMatrix();
  
  
  glTranslatef(0, 0, -(sin(tT)*sin(tT)) * 3);
  
  glmDraw( chuzos, GLM_SMOOTH | GLM_MATERIAL);
  
	glPopMatrix();
}

void Chuzos::refresh(float deltaTime){
  tT += deltaTime;
}


