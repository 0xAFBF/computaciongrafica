//
//  Tronco.cpp
//  ComputacionGrafica
//
//  Created by Andres Botero on 5/15/14.
//  Copyright (c) 2014 BoteRock. All rights reserved.
//

#include "Tronco.h"


Tronco::Tronco()
{
  tronco = glmReadOBJ("trap_tronco.obj");
}

void Tronco::draw(){
  glPushMatrix();
  
  
  glTranslatef(-40.2634, sin(3 * tT)*2.5, -1.8659);
  
  glRotatef(tT * 360, 0, 0, 1);
  
  glTranslatef(40.2634, 3.5331, 1.8659);
  
  glmDraw( tronco, GLM_SMOOTH | GLM_MATERIAL);
  
	glPopMatrix();
}

void Tronco::refresh(float deltaTime){
  tT += deltaTime;
}


