//
//  Mundo.h
//  ComputacionGrafica
//
//  Created by Andres Botero on 5/12/14.
//  Copyright (c) 2014 BoteRock. All rights reserved.
//

#ifndef ComputacionGrafica_Mundo_h
#define ComputacionGrafica_Mundo_h

#include <math.h>

#include "Geom.h"
#include "glm.h"
#include "GameObject.h"
#include "lodepng.h"

class Mundo :public GameObject{
  //  instance variables
  
  
  GLMmodel *mapa;
  //std::vector<unsigned char> img;
  
  
public:
  //  member functions

  Mundo();
  
  void draw();
  void refresh(float deltaTime);
};



#endif
