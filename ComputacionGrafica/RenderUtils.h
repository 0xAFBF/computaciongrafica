//
//  RenderUtils.h
//  ComputacionGrafica
//
//  Created by Andres Botero on 5/7/14.
//  Copyright (c) 2014 BoteRock. All rights reserved.
//


#ifndef ComputacionGrafica_RenderUtils_h
#define ComputacionGrafica_RenderUtils_h

#ifdef __APPLE__
#include <GLUT/GLUT.h>
#else
#include <GL/glut.h>
#endif

#include "Geom.h"

namespace RenderUtils {
  
  
  void glutSetup(int windowX, int windowY, const char* nombre, void (drawfn)(), void (timerfn)(float));
  
  void setCamera(Vector3 from, Vector3 to, float fov, float eyeDist, float targetDist);
  
  void setCamera(Vector3 from, Vector3 to, float fov);
  
  void setCamera(Vector3 from, Vector3 to);
  
  
}

#endif
