//
//  Muros.h
//  ComputacionGrafica
//
//  Created by Andres Botero on 5/15/14.
//  Copyright (c) 2014 BoteRock. All rights reserved.
//

#ifndef __ComputacionGrafica__Muros__
#define __ComputacionGrafica__Muros__


#include <math.h>

#include "Geom.h"
#include "glm.h"
#include "GameObject.h"


class Muros :public GameObject{
  //  instance variables
  
  
  GLMmodel *muros;
  
  float tT = 0.f;
  
  
public:
  //  member functions
  
  Muros();
  
  void draw();
  void refresh(float deltaTime);
};

#endif /* defined(__ComputacionGrafica__Muros__) */
