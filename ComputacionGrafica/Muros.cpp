//
//  Muros.cpp
//  ComputacionGrafica
//
//  Created by Andres Botero on 5/15/14.
//  Copyright (c) 2014 BoteRock. All rights reserved.
//

#include "Muros.h"

Muros::Muros()
{
  muros = glmReadOBJ("trap_muro.obj");
}

void Muros::draw(){
  glPushMatrix();
  
  
  
  glTranslatef(0, 0, (sin(tT)*sin(tT)) * 3);
  
  glmDraw( muros, GLM_SMOOTH | GLM_MATERIAL);
  
	glPopMatrix();
}

void Muros::refresh(float deltaTime){
  tT += deltaTime;
}


