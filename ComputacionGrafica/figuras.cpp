//
//  figuras.cpp
//  ComputacionGrafica
//
//  Created by Andres Botero on 5/7/14.
//  Copyright (c) 2014 BoteRock. All rights reserved.
//

#include "figuras.h"


#ifdef __APPLE__
#include <GLUT/GLUT.h>
#else
#include <GL/glut.h>
#endif

void materialOro()
{
	GLfloat materialColor[] = { 1.f, 0.8f, 0.2f, 1.0f };
	//The specular (shiny) component of the material
	GLfloat materialSpecular[] = { 1.f, 0.8f, 0.2f, 1.0f };
	//The color emitted by the material
	GLfloat materialEmission[] = { 0, 0, 0, 1.0f };
  
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, materialColor);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_EMISSION, materialEmission);
	glMaterialf(GL_FRONT, GL_SHININESS, 10);
}

void materialPlata()
{
	GLfloat materialColor[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	//The specular (shiny) component of the material
	GLfloat materialSpecular[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	//The color emitted by the material
	GLfloat materialEmission[] = { 0, 0, 0, 1.0f };
  
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, materialColor);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_EMISSION, materialEmission);
	glMaterialf(GL_FRONT, GL_SHININESS, 10);
}

void materialCobre()
{
	GLfloat materialColor[] = { 1.0f, 0.5f, 0.2f, 1.0f };
	//The specular (shiny) component of the material
	GLfloat materialSpecular[] = { 1.0f, 0.5f, 0.2f, 1.0f };
	//The color emitted by the material
	GLfloat materialEmission[] = { 0, 0, 0, 1.0f };
  
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, materialColor);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_EMISSION, materialEmission);
	glMaterialf(GL_FRONT, GL_SHININESS, 10);
}

void materialVidrio()
{
	GLfloat materialColor[] = { 0.2f, 0.2f, 1.0f, 1.0f };
	//The specular (shiny) component of the material
	GLfloat materialSpecular[] = {0.5f, 0.5f, 0.5f, 1.0f};
	//The color emitted by the material
	GLfloat materialEmission[] = { 0, 0, 0, 1.0f };
  
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, materialColor);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_EMISSION, materialEmission);
	glMaterialf(GL_FRONT, GL_SHININESS, 10);
}


Vector3 color = {1.f, 1.f, 1.f};

void drawFace(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4)
{
  Vector3 delta1 = { v2.x - v1.x, v2.y - v1.y, v2.z - v1.z };
  Vector3 delta2 = { v3.x - v2.x, v3.y - v2.y, v3.z - v2.z };
  Vector3 delta3 = { v4.x - v3.x, v4.y - v3.y, v4.z - v3.z };
  Vector3 delta4 = { v1.x - v4.x, v1.y - v4.y, v1.z - v4.z };
  
  Vector3 cross1 = crossp(delta1, delta2);
  Vector3 cross2 = crossp(delta2, delta3);
  Vector3 cross3 = crossp(delta3, delta4);
  Vector3 cross4 = crossp(delta4, delta1);
  
  Vector3 crosstot;
  crosstot.x = cross1.x + cross2.x + cross3.x + cross4.x;
  crosstot.y = cross1.y + cross2.y + cross3.y + cross4.y;
  crosstot.z = cross1.z + cross2.z + cross3.z + cross4.z;
  
  float m = sqrt(crosstot.x *crosstot.x + crosstot.y *crosstot.y + crosstot.z *crosstot.z);
  crosstot.x /= m;
  crosstot.y /= m;
  crosstot.z /= m;
  
  /*
   Vector3 sol = { 1.f, 2.f, 3.f };
   float solmag = sqrt(sol.x*sol.x + sol.y*sol.y + sol.z*sol.z);
   float punto = crosstot.x*sol.x + crosstot.y*sol.y + crosstot.z*sol.z;
   float c = punto / solmag * 0.5 + 0.5;
   glColor3f(c * color.x,c * color.y,c * color.z);
   */
  
  
  glBegin(GL_QUADS);
  glNormal3f(crosstot.x, crosstot.y, crosstot.z);
  glVertex3f(v1.x, v1.y, v1.z);
  glVertex3f(v2.x, v2.y, v2.z);
  glVertex3f(v3.x, v3.y, v3.z);
  glVertex3f(v4.x, v4.y, v4.z);
  glEnd();
  
  
  
  //  if (normals->Checked)
  //  {
  //    Vector3 average;
  //    average.x = (v1.x + v2.x + v3.x + v4.x)*0.25;
  //    average.y = (v1.y + v2.y + v3.y +v4.y)*0.25;
  //    average.z = (v1.z + v2.z + v3.z + v4.z)*0.25;
  //
  //    glBegin(GL_LINES);
  //    glVertex3f(average.x, average.y, average.z);
  //    glVertex3f(average.x + crosstot.x, average.y + crosstot.y, average.z + crosstot.z);
  //    glEnd();
  //  }
}

void drawCylinder(float r1, float r2, float r3, float r4, float h, float seg, float theta0, float theta1, bool capTop, bool capBot)
{
  h = h/2;
  
  theta0 *= M_TAU/360;
  theta1 *= M_TAU/360;
  
  float deltaang = (theta1 - theta0) / seg;
  
  for (int i = 0; i < seg; i++)
  {
    float angX = i * deltaang + theta0;
    float angX2 = (i + 1) * deltaang + theta0;
    drawFace(vec3(r1 * cos(angX), r2 * sin(angX), -h),
             vec3(r1 * cos(angX2), r2 * sin(angX2), -h),
             vec3(r3 * cos(angX2), r4 * sin(angX2), h),
             vec3(r3 * cos(angX), r4 * sin(angX), h));
    
    if(capBot)
    {
      drawFace( vec3(r3 * cos(angX2), r4 * sin(angX2), -h),
               vec3(r3 * cos(angX), r4 * sin(angX), -h),
               
               vec3(0,0, -h),
               vec3(0, 0, -h)
               
               );
    }
    if(capTop)
    {
      drawFace(vec3(r1 * cos(angX), r2 * sin(angX), h),
               vec3(r1 * cos(angX2), r2 * sin(angX2), h),
               vec3(0,0, h),
               vec3(0,0, h)
               );
      
    }
    
  }
}

void drawEllipsoid(float rx, float ry, float rz, int divX, int divY, float theta0, float theta1, float phi0, float phi1)
{
  theta0 *= M_TAU/360;
  theta1 *= M_TAU/360;
  phi0 *= M_TAU/360;
  phi1 *= M_TAU/360;
  
  
  float deltaangX = (theta1 - theta0) / divX;
  float deltaangY = (phi1 - phi0) / divY;
  
  for (int i = 0; i < divX; i++)
  {
    float angX = i * deltaangX + theta0;
    float angX2 = (i + 1) * deltaangX + theta0;
    
    for (int j = 0; j < divY; j++)
    {
      float angY = j * deltaangY + phi0 ;
      float angY2 = (j + 1) * deltaangY + phi0 ;
      Vector3 x1 = vec3( rx * cos(angX)*cos(angY), ry * sin(angX)*cos(angY), rz * sin(angY) );
      Vector3 x2 = vec3( rx * cos(angX2)*cos(angY), ry * sin(angX2)*cos(angY), rz * sin(angY) );
      Vector3 x3 = vec3( rx * cos(angX2)*cos(angY2), ry * sin(angX2)*cos(angY2), rz * sin(angY2) );
      Vector3 x4 = vec3( rx * cos(angX)*cos(angY2), ry * sin(angX)*cos(angY2), rz * sin(angY2) );
      drawFace(x1, x2, x3, x4);
      
    }
  }
  
}

void drawTorus(float rx, float ry, int divX, int divY, float theta0, float theta1, float phi0, float phi1)
{
  float deltaangX = (theta1 - theta0)*M_PI/180 / divX;
  float deltaangY = (phi1 - phi0)* M_PI/180  / divY;
  
  for (int i = 0; i < divX; i++)
  {
    float angX = i * deltaangX + theta0 * M_PI / 180;
    float angX2 = (i + 1) * deltaangX + theta0 * M_PI / 180;
    
    for (int j = 0; j < divY; j++)
    {
      float angY = j * deltaangY + phi0 * M_PI / 180;
      float angY2 = (j + 1) * deltaangY + phi0 * M_PI / 180;
      Vector3 x1 = vec3( (rx + ry*sin(angY)) * sin(angX), (rx + ry*sin(angY)) * cos(angX), ry * cos(angY) );
      Vector3 x2 = vec3( (rx + ry*sin(angY))* sin(angX2), (rx + ry*sin(angY))* cos(angX2), ry * cos(angY) );
      Vector3 x3 = vec3( (rx + ry*sin(angY2))* sin(angX2), (rx + ry*sin(angY2))* cos(angX2), ry * cos(angY2) );
      Vector3 x4 = vec3( (rx + ry*sin(angY2))* sin(angX), (rx + ry*sin(angY2))* cos(angX), ry * cos(angY2) );
      drawFace(x1, x2, x3, x4);
      
    }
  }
  
}

void drawRoundedCube( float sizeX, float sizeY, float sizeZ, float radius, int subdivs)
{
  sizeX *= 0.5;
  sizeY *= 0.5;
  sizeZ *= 0.5;
  
  float edgeX = sizeX - radius;
  float edgeY = sizeY - radius;
  float edgeZ = sizeZ - radius;
  
  drawFace(vec3( sizeX, edgeY, edgeZ ),
           vec3( sizeX, -edgeY, edgeZ ),
           vec3( sizeX, -edgeY, -edgeZ ),
           vec3( sizeX, edgeY, -edgeZ ));
  
  drawFace(vec3( -sizeX, -edgeY, edgeZ ),
           vec3( -sizeX, edgeY, edgeZ ),
           vec3( -sizeX, edgeY, -edgeZ ),
           vec3( -sizeX, -edgeY, -edgeZ ));
  drawFace(vec3(-edgeX, sizeY, edgeZ),
           vec3(edgeX, sizeY, edgeZ),
           vec3(edgeX, sizeY, -edgeZ),
           vec3(-edgeX, sizeY, -edgeZ));
  drawFace(vec3(edgeX, -sizeY, edgeZ),
           vec3(-edgeX, -sizeY, edgeZ),
           vec3(-edgeX, -sizeY, -edgeZ),
           vec3(edgeX, -sizeY, -edgeZ)	);
  drawFace(vec3(edgeX, edgeY, sizeZ),
           vec3(-edgeX, edgeY, sizeZ),
           vec3(-edgeX, -edgeY, sizeZ),
           vec3(edgeX, -edgeY, sizeZ)	);
  drawFace(vec3(-edgeX, edgeY, -sizeZ),
           vec3(edgeX, edgeY, -sizeZ),
           vec3(edgeX, -edgeY, -sizeZ),
           vec3(-edgeX, -edgeY, -sizeZ)	);
  
  
  float deltaAng = M_PI/2/subdivs;
  
  for(int i = 0; i<subdivs; i++)
  {
    float i1 = i * deltaAng;
    float i2 = (i + 1)*deltaAng;
    float rcosi1 = radius*cos(i1);
    float rcosi2 = radius*cos(i2);
    float rsini1 = radius*sin(i1);
    float rsini2 = radius*sin(i2);
    
    drawFace(
             vec3( edgeX, edgeY + rcosi1, edgeZ + rsini1 ),
             vec3( -edgeX, edgeY + rcosi1, edgeZ + rsini1 ),
             vec3( -edgeX, edgeY + rcosi2, edgeZ + rsini2 ),
             vec3( edgeX, edgeY + rcosi2, edgeZ + rsini2 ));
    
    drawFace(
             vec3( -edgeX, -(edgeY + rcosi1), (edgeZ + rsini1) ),
             vec3( edgeX, -(edgeY + rcosi1), (edgeZ + rsini1) ),
             vec3( edgeX, -(edgeY + rcosi2), (edgeZ + rsini2) ),
             vec3( -edgeX, -(edgeY + rcosi2), (edgeZ + rsini2) ));
    
    drawFace(
             vec3( edgeX, -(edgeY + rcosi1), -(edgeZ + rsini1) ),
             vec3( -edgeX, -(edgeY + rcosi1), -(edgeZ + rsini1) ),
             vec3( -edgeX, -(edgeY + rcosi2), -(edgeZ + rsini2) ),
             vec3( edgeX, -(edgeY + rcosi2), -(edgeZ + rsini2) ));
    
    drawFace(
             vec3( -edgeX, (edgeY + rcosi1), -(edgeZ + rsini1) ),
             vec3( edgeX, (edgeY + rcosi1), -(edgeZ + rsini1) ),
             vec3( edgeX, (edgeY + rcosi2), -(edgeZ + rsini2) ),
             vec3( -edgeX, (edgeY + rcosi2), -(edgeZ + rsini2) ));
    
    drawFace(
             vec3( (edgeX + rcosi1), -edgeY, (edgeZ + rsini1) ),
             vec3( (edgeX + rcosi1), edgeY, (edgeZ + rsini1) ),
             vec3( (edgeX + rcosi2), edgeY, (edgeZ + rsini2) ),
             vec3( (edgeX + rcosi2), -edgeY, (edgeZ + rsini2) ));
    
    drawFace(
             vec3( -(edgeX + rcosi1), edgeY, (edgeZ + rsini1) ),
             vec3( -(edgeX + rcosi1), -edgeY, (edgeZ + rsini1) ),
             vec3( -(edgeX + rcosi2), -edgeY, (edgeZ + rsini2) ),
             vec3( -(edgeX + rcosi2), edgeY, (edgeZ + rsini2) ));
    
    drawFace(
             vec3( -(edgeX + rcosi1), -edgeY, -(edgeZ + rsini1) ),
             vec3( -(edgeX + rcosi1), edgeY, -(edgeZ + rsini1) ),
             vec3( -(edgeX + rcosi2), edgeY, -(edgeZ + rsini2) ),
             vec3( -(edgeX + rcosi2), -edgeY, -(edgeZ + rsini2) ));
    
    drawFace(
             vec3( (edgeX + rcosi1), edgeY, -(edgeZ + rsini1) ),
             vec3( (edgeX + rcosi1), -edgeY, -(edgeZ + rsini1) ),
             vec3( (edgeX + rcosi2), -edgeY, -(edgeZ + rsini2) ),
             vec3( (edgeX + rcosi2), edgeY, -(edgeZ + rsini2) ));
    
    drawFace(
             vec3( (edgeX + rcosi1), (edgeY + rsini1), edgeZ ),
             vec3( (edgeX + rcosi1), (edgeY + rsini1), -edgeZ ),
             vec3( (edgeX + rcosi2), (edgeY + rsini2), -edgeZ ),
             vec3( (edgeX + rcosi2), (edgeY + rsini2), edgeZ ));
    
    drawFace(
             vec3( -(edgeX + rcosi1), (edgeY + rsini1), -edgeZ ),
             vec3( -(edgeX + rcosi1), (edgeY + rsini1), edgeZ ),
             vec3( -(edgeX + rcosi2), (edgeY + rsini2), edgeZ ),
             vec3( -(edgeX + rcosi2), (edgeY + rsini2), -edgeZ ));
    
    drawFace(
             vec3( -(edgeX + rcosi1), -(edgeY + rsini1), edgeZ ),
             vec3( -(edgeX + rcosi1), -(edgeY + rsini1), -edgeZ ),
             vec3( -(edgeX + rcosi2), -(edgeY + rsini2), -edgeZ ),
             vec3( -(edgeX + rcosi2), -(edgeY + rsini2), edgeZ ));
    
    drawFace(
             vec3( (edgeX + rcosi1), -(edgeY + rsini1), -edgeZ ),
             vec3( (edgeX + rcosi1), -(edgeY + rsini1), edgeZ ),
             vec3( (edgeX + rcosi2), -(edgeY + rsini2), edgeZ ),
             vec3( (edgeX + rcosi2), -(edgeY + rsini2), -edgeZ ));
    
    
    for(int j = 0; j < subdivs; j++)
    {
      float j1 = j*deltaAng;
      float j2 = (j+1)*deltaAng;
      float ri1j1 = rcosi1 * cos(j1);
      float ri1j2 = rcosi1 * cos(j2);
      float ri2j1 = rcosi2 * cos(j1);
      float ri2j2 = rcosi2 * cos(j2);
      float si1j1 = rsini1 * cos(j1);
      float si1j2 = rsini1 * cos(j2);
      float si2j1 = rsini2 * cos(j1);
      float si2j2 = rsini2 * cos(j2);
      float sj1 = radius * sin(j1);
      float sj2 = radius * sin(j2);
      
      
      
      
      drawFace(vec3( (edgeX + ri1j1), (edgeY + si1j1), (edgeZ + sj1) ),
               vec3( (edgeX + ri2j1), (edgeY + si2j1), (edgeZ + sj1) ),
               vec3( (edgeX + ri2j2), (edgeY + si2j2), (edgeZ + sj2) ),
               vec3( (edgeX + ri1j2), (edgeY + si1j2), (edgeZ + sj2) ));
      
      drawFace(vec3( (edgeX + ri1j2), -(edgeY + si1j2), (edgeZ + sj2) ),
               vec3( (edgeX + ri2j2), -(edgeY + si2j2), (edgeZ + sj2) ),
               vec3( (edgeX + ri2j1), -(edgeY + si2j1), (edgeZ + sj1) ),
               vec3( (edgeX + ri1j1), -(edgeY + si1j1), (edgeZ + sj1) ));
      
      drawFace(vec3( (edgeX + ri1j1), -(edgeY + si1j1), -(edgeZ + sj1) ),
               vec3( (edgeX + ri2j1), -(edgeY + si2j1), -(edgeZ + sj1) ),
               vec3( (edgeX + ri2j2), -(edgeY + si2j2), -(edgeZ + sj2) ),
               vec3( (edgeX + ri1j2), -(edgeY + si1j2), -(edgeZ + sj2) ));
      
      drawFace(vec3( (edgeX + ri1j2), (edgeY + si1j2), -(edgeZ + sj2) ),
               vec3( (edgeX + ri2j2), (edgeY + si2j2), -(edgeZ + sj2) ),
               vec3( (edgeX + ri2j1), (edgeY + si2j1), -(edgeZ + sj1) ),
               vec3( (edgeX + ri1j1), (edgeY + si1j1), -(edgeZ + sj1) ));
      
      drawFace(vec3( -(edgeX + ri1j2), (edgeY + si1j2), (edgeZ + sj2) ),
               vec3( -(edgeX + ri2j2), (edgeY + si2j2), (edgeZ + sj2) ),
               vec3( -(edgeX + ri2j1), (edgeY + si2j1), (edgeZ + sj1) ),
               vec3( -(edgeX + ri1j1), (edgeY + si1j1), (edgeZ + sj1) ));
      
      drawFace(vec3( -(edgeX + ri1j1), -(edgeY + si1j1), (edgeZ + sj1) ),
               vec3( -(edgeX + ri2j1), -(edgeY + si2j1), (edgeZ + sj1) ),
               vec3( -(edgeX + ri2j2), -(edgeY + si2j2), (edgeZ + sj2) ),
               vec3( -(edgeX + ri1j2), -(edgeY + si1j2), (edgeZ + sj2) ));
      
      drawFace(vec3( -(edgeX + ri1j2), -(edgeY + si1j2), -(edgeZ + sj2) ),
               vec3( -(edgeX + ri2j2), -(edgeY + si2j2), -(edgeZ + sj2) ),
               vec3( -(edgeX + ri2j1), -(edgeY + si2j1), -(edgeZ + sj1) ),
               vec3( -(edgeX + ri1j1), -(edgeY + si1j1), -(edgeZ + sj1) ));
      
      drawFace(vec3( -(edgeX + ri1j1), (edgeY + si1j1), -(edgeZ + sj1) ),
               vec3( -(edgeX + ri2j1), (edgeY + si2j1), -(edgeZ + sj1) ),
               vec3( -(edgeX + ri2j2), (edgeY + si2j2), -(edgeZ + sj2) ),
               vec3( -(edgeX + ri1j2), (edgeY + si1j2), -(edgeZ + sj2) ));
      
      
    }
  }
}
