//
//  main.cpp
//  ComputacionGrafica
//
//  Created by Andres Botero on 3/6/14.
//  Copyright (c) 2014 BoteRock. All rights reserved.
//

#include <iostream>
#include <math.h>
#include <cstdlib>
#include <vector>

#ifdef __APPLE__
#include <GLUT/GLUT.h>
#else
#include <GL/glut.h>
#endif


//#include "lodepng.h"
//#include "figuras.h"
#include "Character.h"
#include "Mundo.h"
#include "Guillotina.h"
#include "Muros.h"
#include "Chuzos.h"
#include "Tronco.h"


#include "Geom.h"
#include "ViewUtils.h"
#include "RenderUtils.h"

int windowW = 1280;
int windowH = 720;


std::vector<GameObject*> gameObjects;

Character *character ;
Mundo *mundo ;
Guillotina* guill;
Muros* muros;
Chuzos* chuzos;
Tronco *tronco;


void teclado(unsigned char key, int x, int y)
{

  if (key==27) exit(0);

}

void mouse(int button, int state, int x, int y)
{
  
}
void motion(int x, int y)
{
  
}

void joystick(unsigned int botones, int x, int y, int z)
{
  y = -y;
  float dx = x/1000.f;
  float dy = y/1000.f;
  float dz = z/1000.f;
  
  float m = sqrtf(dx * dx + dy * dy);
  
  float angle = atan2(dy, dx) + M_PI*3/4;
  
  
  
  character->move(m * cosf(angle), m *sinf(angle));
}

void init()
{
  character = new Character();
  gameObjects.push_back(character);
  
  
  mundo = new Mundo();
  gameObjects.push_back(mundo);
  
  guill = new Guillotina();
  gameObjects.push_back(guill);
  
  muros = new Muros();
  gameObjects.push_back(muros);
  
  chuzos = new Chuzos();
  gameObjects.push_back(chuzos);
  
  tronco = new Tronco();
  gameObjects.push_back(tronco);
  
}

void timerfn(float deltatime)
{
  for (std::vector<GameObject*>::iterator it = gameObjects.begin(); it != gameObjects.end(); ++it)
  {
    (*it)->refresh(deltatime);
  }
  
  guill->checkDeath(character);
  
  
}

void dibujar()
{
  
  glEnable(GL_LIGHTING);
	
	GLfloat ambientLight[] = { 0.2f, 0.2f, 0.2f, 1.0f };
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientLight);
  
	GLfloat lightColor[] = { 0.5f, 0.7f, 1.f, 1.0f };
	GLfloat lightPos[] = { 1.5f * 5, 2 * 5, 1.5 * 5, 1.0f };
	//Diffuse (non-shiny) light component
	
  glEnable(GL_LIGHT0);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lightColor);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
  
  
  
  GLfloat lightColor2[] = { 1.f, 0.7f, 0.2f, 0.30f };
	
  
  GLfloat lightPos2[] = { character->pos.x  / 10 , 0, 2, 0.00f };
	
  glEnable(GL_LIGHT1);
  glLightfv(GL_LIGHT1, GL_DIFFUSE, lightColor2);
	glLightfv(GL_LIGHT1, GL_SPECULAR, lightColor2);
	glLightfv(GL_LIGHT1, GL_POSITION, lightPos2);
  
  
  RenderUtils::setCamera(vec3(5 + character->pos.x, 5, 5), vec3(character->pos.x, 0, 0));
  
  for (std::vector<GameObject*>::iterator it = gameObjects.begin(); it != gameObjects.end(); ++it)
  {
    glPushMatrix();
    (*it)->draw();
    glPopMatrix();
  }
  //verPlano(true, true, 10.0f, 1.0f);
  
  
}

int main(int argc, char **argv)
{
  
  RenderUtils::glutSetup(windowW, windowH, "Prince of UMNG", dibujar, timerfn);
  
  
  glutKeyboardFunc(teclado);
  glutMouseFunc(mouse);
  glutMotionFunc(motion);
  glutJoystickFunc(joystick, 10);
  
  init();
  
  
  glutMainLoop();
  return 0;
}
