

//
//  Geom.h
//  ComputacionGrafica
//
//  Created by Andres Botero on 5/7/14.
//  Copyright (c) 2014 BoteRock. All rights reserved.
//

#ifndef ComputacionGrafica_Geom_h
#define ComputacionGrafica_Geom_h

#include <math.h>

#ifndef M_PI
#define M_PI 3.14159265358979323846264338327950288
#endif

#define M_TAU 6.28318530717958647692528676655900576


typedef struct Vector3 {
  float x;
  float y;
  float z;
} Vector3;

float clamp(float num, float minimum, float maximum);
bool nearf(float num, float target, float interval);

Vector3 vec3(float x, float y, float z);

Vector3 crossp(Vector3 v1, Vector3 v2);

Vector3 normalize(Vector3 v);

Vector3 delta(Vector3 v1, Vector3 v2);

#endif
