//
//  Mundo.cpp
//  ComputacionGrafica
//
//  Created by Andres Botero on 5/12/14.
//  Copyright (c) 2014 BoteRock. All rights reserved.
//

#include "Mundo.h"



Mundo::Mundo()
{
  mapa = glmReadOBJ("escenario.obj");
  //img = decodeOneStep("stone.png");
  
}

void Mundo::draw(){
  glPushMatrix();
  
//  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
//  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
//  // glTexParameteri( GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_ALPHA );
//  glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA,
//               900, 900, 0,
//               GL_RGBA, GL_UNSIGNED_BYTE, img.data() );
//  
//  
//  
//  glEnable(GL_TEXTURE_2D);
//  
  glmDraw( mapa, GLM_SMOOTH | GLM_MATERIAL | GLM_TEXTURE);
  
	glPopMatrix();
}

void Mundo::refresh(float deltaTime){
  
}


