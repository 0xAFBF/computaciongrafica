//
//  Guillotina.cpp
//  ComputacionGrafica
//
//  Created by Andres Botero on 5/14/14.
//  Copyright (c) 2014 BoteRock. All rights reserved.
//

#include "Guillotina.h"


Guillotina::Guillotina()
{
  guillotina = glmReadOBJ("trap_guillotina.obj");
}

void Guillotina::draw(){
  glPushMatrix();
  
  
  glTranslatef(13.3753, -0.0263, 4.7787);
  
  glRotatef(sin(tT) * 45, 1, 0, 0);
  
  glTranslatef(-13.3753, 0.0263, -4.7787);
  
  glmDraw( guillotina, GLM_SMOOTH | GLM_MATERIAL);
  
	glPopMatrix();
}

void Guillotina::refresh(float deltaTime){
  tT += deltaTime;
}


//#include <iostream>
void Guillotina::checkDeath(Character * character)
{
  //std::cout<<"ola k ase"<<character->pos.x<<std::endl;

  bool nearX = nearf(character->pos.x, -13.37, 0.5);
  bool nearY = nearf(character->pos.y, sin(tT) *2, 2);
  if(nearX && nearY)
  {
    character->reset();
  }
}
