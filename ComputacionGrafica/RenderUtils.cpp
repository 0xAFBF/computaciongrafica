//
//  RenderUtils.cpp
//  ComputacionGrafica
//
//  Created by Andres Botero on 5/7/14.
//  Copyright (c) 2014 BoteRock. All rights reserved.
//

#include "RenderUtils.h"

#include <iostream>


namespace RenderUtils {

  
  
  
  //viewport
  int vpWidth;
  int vpHeight;
  
  //camera
  Vector3 camFrom;
  Vector3 camTo;
  Vector3 camUp;
  Vector3 camRight;
  Vector3 camFwd;
  float fov;
  
  //stereo
  bool stereo;
  float eyeDist;
  float targetDist;
  int leftColor;
  int rightColor;
  bool flipped;
  
  //pointers to functions
  void (*drawFunc)();
  void (*timerFunc)(float);
  
  float currentTime;
  
  
  
 
  void posCamara( Vector3 from, Vector3 to,Vector3 up, float fovY)
  {
    
    glPushMatrix ();
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective( fovY, (float)vpWidth/(float)vpHeight, 1.0, 100.0 );
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    gluLookAt(   from.x,from.y,from.z,    to.x,to.y,to.z,   up.x, up.y, up.z  );
    
  }
  
  void prerender()
  {
    glColorMask( GL_TRUE,GL_TRUE,GL_TRUE, GL_TRUE);
    
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
    glDepthFunc(GL_LEQUAL);
    glDepthRange(0.0f, 1.0f);
    
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClearDepth(1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glEnable(GL_CULL_FACE);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    //glBlendFunc(GL_ONE, GL_ONE);
    
    glEnable( GL_BLEND );
  }
  void cleanup()
  {
    glColorMask( GL_TRUE,GL_TRUE,GL_TRUE, GL_TRUE);
    glClear (GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glMatrixMode(GL_MODELVIEW);
  }
  
  void render()
  {
    GLenum err = glGetError();
    if(err)
    {
      std::cout<<"OpenGL Error>"<<err<<std::endl;
    }
    glFlush();
    glutSwapBuffers();
  }
  
  inline void callDrawFunc()
  {
    glPushMatrix();
    drawFunc();
    glPopMatrix();
  }
  
  void vista3D ()
  {
    float prevTime = currentTime;
    currentTime = (float)glutGet(GLUT_ELAPSED_TIME)/1000.f;
    
    float deltaTime = currentTime - prevTime;
    
    timerFunc(deltaTime);
    
    
    
    prerender();
    
    camFwd = delta(camFrom, camTo);
    camRight = normalize(crossp(camFwd, vec3(0, 0, 1)));
    camUp = normalize(crossp(camRight, camFwd));
    
    if (!stereo)
    {
      cleanup();
      posCamara( camFrom, camTo, camUp , 60.0 );
      callDrawFunc();
      
    }
    else
    {
      
      Vector3 leftCam;
      leftCam.x = camFrom.x - camRight.x * eyeDist * 0.5;
      leftCam.y = camFrom.y - camRight.y * eyeDist * 0.5;
      leftCam.z = camFrom.z - camRight.z * eyeDist * 0.5;
      
      Vector3 rightCam;
      rightCam.x = camFrom.x + camRight.x * eyeDist * 0.5;
      rightCam.y = camFrom.y + camRight.y * eyeDist * 0.5;
      rightCam.z = camFrom.z + camRight.z * eyeDist * 0.5;
      
      Vector3 leftTgt;
      leftTgt.x = camTo.x - camRight.x * targetDist * 0.5;
      leftTgt.y = camTo.y - camRight.y * targetDist * 0.5;
      leftTgt.z = camTo.z - camRight.z * targetDist * 0.5;
      
      Vector3 rightTgt;
      rightTgt.x = camTo.x + camRight.x * targetDist * 0.5;
      rightTgt.y = camTo.y + camRight.y * targetDist * 0.5;
      rightTgt.z = camTo.z + camRight.z * targetDist * 0.5;
      
      int lcol = leftColor;
      int rcol = rightColor;
      
      if(flipped)
      {
        int a = lcol;
        lcol =  rcol;
        rcol = a;
      }
      
#warning remove maybe
      //glColorMask(1,1,1, GL_TRUE);
      
      
      cleanup();
      glColorMask(lcol>>2, lcol>>1&1, lcol&1, GL_TRUE);
      
      posCamara( leftCam, leftTgt, camUp , fov);
      callDrawFunc();
      
      cleanup();
      glColorMask(rcol>>2, rcol>>1&1, rcol&1, GL_TRUE);
      posCamara( rightCam, rightTgt, camUp, fov);
      callDrawFunc();
      
    }
    //end render loop
    render();
    
    glutPostRedisplay();
  }
  

  
  void menu(int id)
  {
    //std::cout<<"id:"<<id<<std::endl;
    if(id == 0)
    {
      stereo = !stereo;
    }
    else if(id == 1)
    {
      leftColor = 3; rightColor = 4;
    }
    else if(id == 2)
    {
      leftColor = 2; rightColor = 5;
    }
    else if(id == 3)
    {
      leftColor = 4; rightColor = 1;
    }
    else if(id == 4)
    {
      leftColor = 4; rightColor = 2;
    }
    else if(id == 5)
    {
      flipped = flipped?false:true;
      
    }
  }
  
  void addMenus()
  {
    glutCreateMenu(menu);
    
    glutAddMenuEntry("Enable/Disable Stereo", 0);
    glutAddMenuEntry("Red/Cyan", 1);
    glutAddMenuEntry("Green/Magenta", 2);
    glutAddMenuEntry("Red/Blue", 3);
    glutAddMenuEntry("Red/Green", 4);
    glutAddMenuEntry("Flip Sides", 5);
    
    glutAttachMenu(GLUT_RIGHT_BUTTON);
  }
  
  void initDefaults()
  {
    camFrom = vec3(5, 5, 5);
    camTo = vec3(0, 0, 0);
    stereo = false;
    eyeDist = 0.1;
    targetDist = 0;
    leftColor = 3; rightColor = 4;
    fov = 60;
  }
  void initRest()
  {
    //glEnable(GL_MULTISAMPLE);
  }
  
  void glutSetup(int windowX, int windowY, const char* nombre, void (drawfn)(), void (timerfn)(float))
  {
    vpWidth = windowX;
    vpHeight = windowY;
    drawFunc = drawfn;
    timerFunc = timerfn;
    initDefaults();
    
    glutInitWindowSize( vpWidth, vpHeight );
    char *myargv [1];
    int myargc=1;
    myargv [0]=strdup ("Myappname");
    glutInit(&myargc, myargv);
    glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB  | GLUT_DEPTH);
//    glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB  | GLUT_DEPTH | GLUT_MULTISAMPLE);
    glutCreateWindow("Demo OpenGL");
    addMenus();
    initRest();
    
    glutDisplayFunc(vista3D);
    
  }
  
  void setCamera(Vector3 _from, Vector3 _to, float _fov, float _eyeDist, float _targetDist)
  {
    camFrom = _from;
    camTo = _to;
    fov = _fov;
    eyeDist = _eyeDist;
    targetDist = _targetDist;
  }
  
  void setCamera(Vector3 _from, Vector3 _to, float _fov)
  {
    camFrom = _from;
    camTo = _to;
    fov = _fov;
  }
  
  void setCamera(Vector3 _from, Vector3 _to)
  {
    camFrom = _from;
    camTo = _to;
  }
  
}
