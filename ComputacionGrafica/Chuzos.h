//
//  Chuzos.h
//  ComputacionGrafica
//
//  Created by Andres Botero on 5/15/14.
//  Copyright (c) 2014 BoteRock. All rights reserved.
//

#ifndef __ComputacionGrafica__Chuzos__
#define __ComputacionGrafica__Chuzos__

#include <math.h>

#include "Geom.h"
#include "glm.h"
#include "GameObject.h"


class Chuzos :public GameObject{
  //  instance variables
  
  
  GLMmodel *chuzos;
  
  float tT = 0.f;
  
  
public:
  //  member functions
  
  Chuzos();
  
  void draw();
  void refresh(float deltaTime);
};

#endif /* defined(__ComputacionGrafica__Chuzos__) */
